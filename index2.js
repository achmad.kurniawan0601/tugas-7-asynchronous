var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise
const waktu = 10000;
readBooksPromise(waktu, books[0]).then(waktu => {
    return readBooksPromise(waktu, books[1])
})
.then(waktu => {
    return readBooksPromise(waktu, books[2])
})
.then (waktu => {
    return readBooksPromise(waktu, books[3])
})
